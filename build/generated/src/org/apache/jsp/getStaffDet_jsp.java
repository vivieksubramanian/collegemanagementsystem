package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class getStaffDet_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Skcet Management</title>\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <!-- <link rel=\"stylesheet\" type=\"text/css\"  href=\"newcss.css\">-->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <script src=\"Jquery/jquery.min.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\"  href=\"styles.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        <div class=\"jumbotron\" style=\"height: 150px;background-color: #777;\">\n");
      out.write("            <div>\n");
      out.write("                <img src=\"logo1.png\" alt=\"mickey\" style=\"height: 120px;width: 450px;position: absolute;top:15px;left: 20px\">\n");
      out.write("            </div>\n");
      out.write("            <div style=\"position: absolute;right: 150px;top: 70px;\">\n");
      out.write("                <a href=\"index.jsp\"><button  type=\"button\" class=\"btn btn-primary\">HOME</button></a>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"position: absolute;right: 50px;top: 70px;\">\n");
      out.write("                <a href=\"#\"><button  type=\"button\" class=\"btn btn-primary\">FAQ</button></a>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-2\" style=\"position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;\">\n");
      out.write("                    <a href=\"firstjsp.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 50px;\">RULES AND REGULATIONS</button></a>\n");
      out.write("                    <a href=\"register.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 90px;\">REGISTRATON</button></a>\n");
      out.write("                    <div class=\"dropdown\" style=\"position: absolute;left: 15px;top: 130px\">\n");
      out.write("                        <button class=\"btn btn-primary dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" style=\"width: 250px;position: absolute;\">VIEW DETAILS\n");
      out.write("                            <span class=\"caret\"></span></button>\n");
      out.write("                        <ul class=\"dropdown-menu\" style=\"position: absolute;top: 30px;width: 250px;\">\n");
      out.write("                            <li><a href=\"getStuDet.jsp\">STUDENT</a></li>\n");
      out.write("                            <li><a href=\"getStaffDet.jsp\">STAFF</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                    <a href=\"upload_qb.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 170px;\">UPLOAD QUESTION BANK</button></a>\n");
      out.write("                    <a href=\"index.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 210px;\">LOGOUT</button></a>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-lg-10\" style=\"position: absolute;left: 284px;height: 582px;background-image: url(p1.jpeg);top: 150px;\">\n");
      out.write("                   \n");
      out.write("                       \n");
      out.write("                   <form action=\"staffdet.jsp\" name=\"username\" method=\"post\" style=\"max-width:500px;height: 532px;margin: auto;padding-top: 0px;\">\n");
      out.write("                        <h1 style=\"color: #00ffff;\"><b>SELECT DEPARTMENT</b></h1>\n");
      out.write("                    <div class=\"form-group\">\n");
      out.write("                        <input class=\"form-control\" type=\"text\" name=\"department\" placeholder=\"DEPARTMENT\" size=\"50\" required />\n");
      out.write("                    </div>\n");
      out.write("                    \n");
      out.write("                    <input class=\"btn btn-group-lg btn-success\" type=\"submit\" value=\"GET DETAILS\" name=\"login\" />\n");
      out.write("                    <input class=\"btn btn-group-lg btn-success\" type=\"reset\" value=\"RESET\" />\n");
      out.write("                </form>\n");
      out.write("                \n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <footer >\n");
      out.write("            &copy COPYRIGHTS RESERVED\n");
      out.write("        </footer>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
