<%-- 
    Document   : student_login
    Created on : 12 Nov, 2019, 11:03:11 PM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Skcet Management</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>
        <header>
            <div>
            <img id="myImg" src="logo1.png" alt="mickey">
            </div>
            <div><a href="index.jsp"><button id="vis" type="submit" name="HOME">HOME</button></a>
            </div>
            <div class="dropdown">
                <button class="dropbtn">LOGIN</button>
                <div class="dropdown-content">
                <a href="login.jsp">ADMIN</a>
                <a href="student_login.jsp">STUDENT</a>
                <a href="#">STAFF</a>
                </div>
             </div>
            <div>
                <a href="#"><button id="hit" type="submit" name="FAQ" >FAQ</button></a>
            </div>
        </header>
        <section>
        <article id="log">
            <div >
                <center>
                    <h1 style="color: #00ffff;"><b>STUDENT LOGIN FORM</b></h1>
                <form action="db_studentlogin.jsp" name="username" method="post">
                    <div class="form-group">
                      <input class="form-control"  type="text" name="name"  placeholder="USER NAME" size="50" style="width: 20%;" required/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" name="pass" placeholder="PASSWORD" size="50" style="width: 20%" required />
                    </div>
                    <input class="btn btn-group-lg btn-success" type="submit" value="LOGIN" name="login" />
                    <input class="btn btn-group-lg btn-success" type="reset" value="RESET" />
                </form>
                </center>
            </div>
        </article>
         </section>
        <footer>
                
             &copy copyrights reserved 
                
        </footer>
        
    </body>
</html>
