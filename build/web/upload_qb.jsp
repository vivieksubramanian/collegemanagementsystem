<%-- 
    Document   : upload_qb
    Created on : 16 Mar, 2020, 8:09:29 PM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Skcet Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>

        <div class="jumbotron" style="height: 150px;background-color: #777;">
            <div>
                <img src="logo1.png" alt="mickey" style="height: 120px;width: 450px;position: absolute;top:15px;left: 20px">
            </div>
            <div style="position: absolute;right: 150px;top: 70px;">
                <a href="index.jsp"><button  type="button" class="btn btn-primary">HOME</button></a>
            </div>
            <div style="position: absolute;right: 50px;top: 70px;">
                <a href="#"><button  type="button" class="btn btn-primary">FAQ</button></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2" style="position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;">
                     <a href="firstjsp.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 50px;">RULES AND REGULATIONS</button></a>
                    <a href="register.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 90px;">REGISTRATON</button></a>
                    <div class="dropdown" style="position: absolute;left: 15px;top: 130px">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 250px;position: absolute;">VIEW DETAILS
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu" style="position: absolute;top: 30px;width: 250px;">
                            <li><a href="getStuDet.jsp">STUDENT</a></li>
                            <li><a href="getStaffDet.jsp">STAFF</a></li>
                        </ul>
                    </div>
                    <a href="upload_qb.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 170px;">UPLOAD QUESTION BANK</button></a>
                    <a href="admin_view_qp_deptwise.jsp"><button class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 210px;">VIEW QUESTION PAPERS</button></a>
                    <a href="index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 250px;">LOGOUT</button></a>
                </div>
                <div class="col-lg-10" style="position: absolute;left: 284px;height: 582px;background-image: url(p1.jpeg);top: 150px;" >
                    <form  action="db_qb_upload" enctype="multipart/form-data" style="max-width:500px;height: 532px;margin: auto;padding-top: 0px;" method="post">
                        <h1 style="color: #00ffff"><b>UPLOAD QUESTION BANK</b></h1>
                        
                         <div class="form-group"> 
                            <input class="form-control" type="text" placeholder="department*" name="department" required>
                        </div>
                        
                         <div class="form-group"> 
                            <input class="form-control" type="text" placeholder="subject*" name="subject" required>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="file" name="file" required>
                        </div>
                        
                        <input class="btn btn-group-lg btn-success" type="reset" value="clear">
                        <button type="submit" class="btn btn-group-lg btn-success">UPLOAD</button>
                    </form>
                    
                </div>
            </div>
        </div>
        <footer >
            &copy COPYRIGHTS RESERVED
        </footer>
    </body>
</html>

