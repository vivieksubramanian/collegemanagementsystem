<%-- 
    Document   : staff_index
    Created on : 1 Dec, 2019, 7:59:13 PM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>Skcet Managment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>
        <div class="jumbotron" style="height: 150px;background-color: #777;">
            <div>
                <img src="logo1.png" alt="mickey" style="height: 120px;width: 450px;position: absolute;top:15px;left: 20px">
            </div>
            <div style="position: absolute;right: 150px;top: 70px;">
                <a href="index.jsp"><button  type="button" class="btn btn-primary">HOME</button></a>
            </div>
            <div style="position: absolute;right: 50px;top: 70px;">
                <a href="#"><button  type="button" class="btn btn-primary">FAQ</button></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2" style="position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;">
                    <a href="staff_index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 50px;">RULES AND REGULATIONS</button></a>
                    <a href="staff_validate.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 90px;">MY PROFILE</button></a>
                    <a href="staff_student_details.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 130px;">STUDENT DETAILS</button></a>
                    <a href="staff_qb_upload.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 170px;">UPLOAD QUESTION BANK</button></a>
                    <a href="staff_view_qp_deptwise.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 210px;">VIEW QUESTION PAPERS</button></a>
                    <a href="index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 250px;">LOGOUT</button></a>
                </div>
                <div class="col-lg-10" style="position: absolute;left: 284px;height: 582px;background-image: url(p1.jpeg);top: 150px;overflow: scroll;">
                   <div>
                        <h1  style="position: absolute;left: 400px;color: #00ffff;"><B>RULES AND REGULATIONS</B></h1>
                    </div>
                    <div>
                        <p style="position: absolute;top:15%;color: white">
                            <b style="color: #00ffff">RULES AND REGULATIONS FOR COLLEGE</b><br>
                            -> The college gives utmost priority for discipline and every one, student or staff, is bound to follow the rules and regulations of the college and maintain strict discipline.<br>
                            -> Under disciplinary action, the principal is empowered to fine, suspend or even expel a student from the college in the interests of the institution.<br>
                            -> Students are not permitted to possess or use Mobile Phones inside the college campus.<br>
                            -> BROWSING IS NOT ALLOWED DURING LAB HOURS. Staff and students are not allowed to misuse the internet facilities.<br>
                            -> No one will be allowed to listen to Music from any device inside the college campus.<br>
                            -> Every Student shall conduct himself / herself in such a way to cause no disturbance to the working of the classes or to fellow students.<br>
                            -> RAGGING, CONSUMING ALCOHOL AND SMOKING ARE STRICTLY PROHIBITED IN THE HOSTEL AND COLLEGE CAMPUS.<br>
                            -> No function shall be arranged by the students in the hostel or college campus without prior permission from the principal.<br>
                            -> Writing on walls, pillars, bath rooms, furniture or black boards is strictly prohibited.<br>
                            -> Eating snacks / taking lunch inside the class rooms or along corridors are not permitted.<br>
                            -> The cost of any damage, if caused to college property will be charged to the accounts of the students responsible for the damage in addition to disciplinary action.<br>
                            -> Students are advised to switch off fans and lights when they leave the class rooms.<br>
                            -> Furniture in the class rooms should not be moved or displaced.<br>
                            -> Students are not allowed to attend classes or any college function wearing shorts, dhotis, jeans or T-Shirts.<br>
                            -> No one will be allowed to whistle, hoot, shout or sing aloud while travelling in the bus. They are also not allowed to throw papers or other articles while travelling.<br>
                            -> No students shall remain in the hostel during class – hours unless he / she is sick and is permitted to be on leave.<br>
                            -> Students have to take without fail all the necessary items such as hall ticket, admit card, pen, pencil, calculator, Scale, rubber, etc., to the test / exam hall.<br>
                            -> Students will have to sit through the entire period scheduled for the internal tests and university examinations.<br>
                            -> In the event of continued poor performance in internal tests and poor class attendance by any student, the principal has the authority to withhold permission for him/her to write the university examinations.<br><br>
                            <b style="color: #00ffff">RULES AND REGULATIONS FOR LIBRARY</b><br>
                            -> Silence must be observed in the Library.<br>
                            -> Personal belongings are not allowed inside the Library.<br>
                            -> Every staff / student of the college is eligible for membership of the Library.<br>
                            -> The Library can be utilized by the students and staff from 9 A.M. to 7 P.M. on working days.<br>
                            -> All students should sign the entry register of the Library, before entering.<br>
                            -> Books borrowing limit: Faculty – 5, Non Teaching staff – 2, Student – 3.<br>
                            -> Principal is empowered to increase additional issue of books to whomever he feels necessary.<br>
                            -> Borrower’s tickets are not transferable.<br>
                            -> Books are to be handled very carefully. If a book is lost by the student, he/she shall replace the book (same title, author and edition) or shall pay double the cost of the book as fine.<br>
                            -> Members before leaving the issue counter must satisfy themselves as to whether the books which they intend to borrow are in good condition and any damage should be immediately.<br> Reported to the Librarian or library staff, failing which the member to whom the book was issued will be held responsible.<br>
                            -> Members are not permitted to underline, write in, folding / tearing of pages or defaced books in any way whatsoever.<br>
                            -> The borrowed book should be returned on or before due date, If not, overdue charge of Rs.2. per day for students will be collected.<br>
                            -> If the due date falls on holidays, return can be done on the following working day without fine.<br>
                            -> Misbehavior in the library will lead to cancellation of membership and serious disciplinary action.<br>
                            -> A member who has lost borrower’s token shall make a written report to the librarian, then original or duplicate library token will be issued on payment of Rs. 50/-.<br>
                            -> The members will be responsible for any loss or non return of books issued against their original or duplicate borrower’s ticket.<br>
                            -> All final year students should return their library token and library book based on library circular and obtain “NO DUE CERTIFICATE” from the library for getting Hall ticket.<br>
                            -> Similarly the staff members who intend to leave the college should settle all the dues and obtain “NO DUE CERTIFICATE” from the library.<br><br>
                            <b style="color: #00ffff">RULES AND REGULATIONS FOR HOSTEL</b><br>
                            -> These rules and regulations are complementary to the rules and regulations of the college.<br>
                            -> Inmates of the hostels should strictly abide by the rules and regulations of the hostels. A declaration to this effect should be signed at the time of admission.<br>
                            -> The Warden / Dy. Warden shall act as the local guardian of the inmates of the hostel.<br>
                            -> No student is permitted to leave the hostel without the consent of parents and without permission from the Warden.<br>
                            -> While going outside the hostel premises, the inmates should maintain discipline and their behavior and conduct should be impeccable so as to keep up the honour and prestige of the College.<br> If there is any complaint against any of the inmates regarding his / her misbehavior, severe disciplinary action will be taken, which may even lead to expulsion from the Hostel or College.<br>
                            -> The principal is vested with powers to regulate the hostel admission assisted by the respective wardens and to enforce general discipline in the hostels. Admission may be refused to any student seeking the same, and any inmate may be dismissed without assigning reason, if they indulge with any gross misbehavior.<br>
                            -> The mess fees shall be paid on or before 15th of every month or with – one week after the mess charges for the previous month are put up on the Notice Board.<br> No reduction in mess charges is permitted for leave of absence for less than three days.<br> If the absence is continuous for three days or more with prior permission, reduction of mess charges will be granted.<br> However, no one such reduction will be given for absence without permission.<br> Only one such reduction per month is permitted for each student.<br> To get the benefit of reduction the inmate should give a written request to the warden / deputy warden. <br>On returning from leave he / she should report to the warden / deputy warden.<br>
                            -> Inmates are not permitted to organize any picnic or pleasure trip without the permission of the principal. No unauthorized meetings or activities are permitted in the premises of the hostels.<br> Any such meeting or activity will be viewed very seriously and action which may include dismissal from the Hostel will be taken against the organizers.<br>
                            -> Guests / day scholars are not allowed to stay in the hostels. Action will be taken against if any guest / day scholar is found in the rooms.<br>
                            -> Rooms shall be kept neat and tidy. The inmates are responsible for the care of the furniture and fittings in their respective rooms.<br> For loss or damage to hostel property the inmates will be penalized and the cost will be recovered from them.<br>
                            -> No student will be allowed to use the hostel phone unless it is very essential. If it is essential students can use the phone with the permission of the warden1Incoming calls will be allowed up to 10 P.M. only.<br>
                            -> The incoming calls will be intimated to the inmates only if the Warden is convinced of the identity of the person calling.<br>
                            -> The study hours will be from 07.00 P.M. to 08.00 P.M. and from 08.45 P.M. to 09.45 P.M. During study hours visiting other rooms should be avoided.<br>
                            -> Visitors will be permitted only on Sundays and other holidays between 7.00 A.M. and 06.30 P.M.<br>
                            -> In order to reach the classes in time, all students residing in the hostels should start from the hostel at 10 minutes before the stroke of the bell both in the forenoon and in the afternoon. Late arrival in the class will be viewed seriously and such students may be asked to vacate the hostel in the event of repeated violation of the rule.<br>
                            -> The use of tape recorders will not be allowed in the hostel.<br>
                            -> Computer without CD drives will be allowed to be used in the hostel with permission from the Principal. The software stored should have been approved by the HOD and Warden.<br>
                            -> If any student goes for Markas (Thursday evening prayer meeting), the student should intimate the same in writing to the Warden / Dy. Warden sufficiently in advance.<br><br>
                            <b style="color: #00ffff">RULES AND REGULATIONS FOR INTERNET LAB</b><br>
                            -> Access to the Internet is a privilege, not a right.<br>
                            -> Students should enter the log-in and log-out time in the log note without fail.<br>
                            -> Students must produce Identity Card when demanded, to utilize the lab resources.<br>
                            -> Students are not allowed to download pictures, music, videos or files without the permission of a staff.<br>
                            -> Log-on with your username and password for your use only. Never share your username and password.<br>
                            -> Food or drinks are not allowed to bring inside the computer lab.<br>
                            -> Do not install software’s without permission.<br>
                            -> Do not remove or disconnect parts, cables, or labels.<br>
                            -> Site includes chat rooms, instant messaging (IM), social and adult sites are strictly prohibited.<br>
                            -> No Internet/Intranet gaming activities allowed.<br>
                            -> Do not personalize the computer settings. (This includes desktop, screen saver, etc.)<br>
                            -> Ask permission to print.<br>
                            -> Maintain silence inside the lab.<br>
                            -> Log-off — leave the computer ready for the next person to use. Pick-up your materials and push in the chair.<br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <footer >
            &copy COPYRIGHTS RESERVED
        </footer>
    </body>
</html>
