<%-- 
    Document   : staff_upload_qb_success
    Created on : 13 Apr, 2020, 7:02:58 AM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Skcet Managment</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>
        <div class="jumbotron" style="height: 150px;background-color: #777;">
            <div>
                <img src="logo1.png" alt="mickey" style="height: 120px;width: 450px;position: absolute;top:15px;left: 20px">
            </div>
            <div style="position: absolute;right: 150px;top: 70px;">
                <a href="index.jsp"><button  type="button" class="btn btn-primary">HOME</button></a>
            </div>
            <div style="position: absolute;right: 50px;top: 70px;">
                <a href="#"><button  type="button" class="btn btn-primary">FAQ</button></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2" style="position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;">
                  <a href="staff_index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 50px;">RULES AND REGULATIONS</button></a>
                    <a href="staff_validate.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 90px;">MY PROFILE</button></a>
                    <a href="staff_student_details.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 130px;">STUDENT DETAILS</button></a>
                    <a href="staff_qb_upload.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 170px;">UPLOAD QUESTION BANK</button></a>
                    <a href="staff_view_qp_deptwise.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 210px;">VIEW QUESTION PAPERS</button></a>
                    <a href="index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 250px;">LOGOUT</button></a>
                </div>
                <div class="col-lg-10" style="position: absolute;left: 284px;height: 582px;background-image: url(p1.jpeg);top: 150px;">
                    <center>
                        <c:if test="${requestScope.msg!=null}">
                            <h2 style="color: white"><c:out value="${requestScope.msg}"></c:out></h2>
                        </c:if><br><br>
                        <c:if test="${sessionScope.filename!=null}">
                            <c:set var="file" scope="session" value="${sessionScope.filename}"/>
                        </c:if>
                        <a href="<c:url value="db_download_qbSf?filename=${file}"/>"><button class="btn btn-group-lg btn-primary">DOWNLOAD</button></a>&nbsp;&nbsp;&nbsp;
                        <a href="<c:url value="staff_db_view_qb.jsp"/>"><button class="btn btn-group-lg btn-primary">VIEW ALL QUESTION BANK</button></a><br>
                        <div>
                        <a href="staff_qb_upload.jsp"><button class="btn btn-group-lg btn-warning">EXIT</button></a>
                        </div>
                    </center>
                </div>
            </div>
        </div>
        <footer >
            &copy COPYRIGHTS RESERVED
        </footer>
    </body>
</html>

