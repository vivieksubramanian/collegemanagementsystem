package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class student_005findex_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Skcet Managment</title>\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("        <!-- <link rel=\"stylesheet\" type=\"text/css\"  href=\"newcss.css\">-->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("        <script src=\"Jquery/jquery.min.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\"  href=\"styles.css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"jumbotron\" style=\"height: 150px;background-color: #777;\">\n");
      out.write("            <div>\n");
      out.write("                <img src=\"logo1.png\" alt=\"mickey\" style=\"height: 120px;width: 450px;position: absolute;top:15px;left: 20px\">\n");
      out.write("            </div>\n");
      out.write("            <div style=\"position: absolute;right: 150px;top: 70px;\">\n");
      out.write("                <a href=\"index.jsp\"><button  type=\"button\" class=\"btn btn-primary\">HOME</button></a>\n");
      out.write("            </div>\n");
      out.write("            <div style=\"position: absolute;right: 50px;top: 70px;\">\n");
      out.write("                <a href=\"#\"><button  type=\"button\" class=\"btn btn-primary\">FAQ</button></a>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-2\" style=\"position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;\">\n");
      out.write("                    <a href=\"firstjsp.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 50px;\">RULES AND REGULATIONS</button></a>\n");
      out.write("                    <a href=\"student_validate.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 90px;\">MY PROFILE</button></a>\n");
      out.write("                    \n");
      out.write("                    <a href=\"index.jsp\"><button type=\"button\" class=\"btn btn-group-lg btn-primary\" style=\"width: 250px;position: absolute;top: 130px;\">LOGOUT</button></a>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"col-lg-10\" style=\"position: absolute;left: 284px;height: 582px;background-image: url(p1.jpeg);top: 150px;\">\n");
      out.write("                    <h1  style=\"position: absolute;left: 400px;color: #00ffff;\">STUDENT INFORMATION</h1>\n");
      out.write("                    <p  style=\"position: absolute;left: 400px;color: #ffff00;top: 100px\">\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("                        *<br>\n");
      out.write("\n");
      out.write("                    </p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <footer >\n");
      out.write("            &copy COPYRIGHTS RESERVED\n");
      out.write("        </footer>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
