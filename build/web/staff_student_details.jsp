<%-- 
    Document   : staff_student_details
    Created on : 12 Apr, 2020, 2:05:17 AM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Skcet Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>

        <div class="jumbotron" style="height: 150px;background-color: #777;">
            <div>
                <img src="logo1.png" alt="mickey" style="height: 120px;width: 450px;position: absolute;top:15px;left: 20px">
            </div>
            <div style="position: absolute;right: 150px;top: 70px;">
                <a href="index.jsp"><button  type="button" class="btn btn-primary">HOME</button></a>
            </div>
            <div style="position: absolute;right: 50px;top: 70px;">
                <a href="#"><button  type="button" class="btn btn-primary">FAQ</button></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2" style="position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;">
                    <a href="staff_index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 50px;">RULES AND REGULATIONS</button></a>
                    <a href="staff_validate.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 90px;">MY PROFILE</button></a>
                    <a href="staff_student_details.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 130px;">STUDENT DETAILS</button></a>
                    <a href="staff_qb_upload.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 170px;">UPLOAD QUESTION BANK</button></a>
                    <a href="staff_view_qp_deptwise.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 210px;">VIEW QUESTION PAPERS</button></a>
                    <a href="index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 250px;">LOGOUT</button></a>
                </div>
                <div class="col-lg-10" style="position: absolute;left: 284px;height: 582px;background-image: url(p1.jpeg);top: 150px;">
                   
                        
                   <form action="staff_viewStudent_details.jsp" name="username" method="post" style="max-width:500px;height: 532px;margin: auto;padding-top: 0px;">
                       <h1 style="color: #00ffff;"><b>SELECT DEPARTMENT</b></h1>
                    <div class="form-group" >
                        <input class="form-control" type="text" name="department" placeholder="DEPARTMENT" size="50" required />
                    </div>
                    <input class="btn btn-group-lg btn-success" type="submit" value="GET DETAILS" name="login" />
                    <input class="btn btn-group-lg btn-success" type="reset" value="RESET" />
                </form>
                </div>
            </div>
        </div>
        <footer >
            &copy COPYRIGHTS RESERVED
        </footer>
    </body>
</html>

