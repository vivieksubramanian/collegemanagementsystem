<%-- 
    Document   : db_view_qp
    Created on : 12 Apr, 2020, 1:12:57 AM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*" %>
<%@page import="com.servlet.db.DB" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Skcet Management</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>
        <%!
            Connection con=null;
            PreparedStatement ps=null;
            ResultSet rs=null;
            %>
        <div class="jumbotron" style="height: 150px;background-color: #777;">
            <div>
                <img src="logo1.png" alt="mickey" style="height: 120px;width: 450px;position: absolute;top:15px;left: 20px">
            </div>
            <div style="position: absolute;right: 150px;top: 70px;">
                <a href="index.jsp"><button  type="button" class="btn btn-primary">HOME</button></a>
            </div>
            <div style="position: absolute;right: 50px;top: 70px;">
                <a href="#"><button  type="button" class="btn btn-primary">FAQ</button></a>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2" style="position: absolute;left: 0px;height: 582px;background-color: #cccccc;top: 150px;">
                     <a href="firstjsp.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 50px;">RULES AND REGULATIONS</button></a>
                    <a href="register.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 90px;">REGISTRATON</button></a>
                    <div class="dropdown" style="position: absolute;left: 15px;top: 130px">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 250px;position: absolute;">VIEW DETAILS
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu" style="position: absolute;top: 30px;width: 250px;">
                            <li><a href="getStuDet.jsp">STUDENT</a></li>
                            <li><a href="getStaffDet.jsp">STAFF</a></li>
                        </ul>
                    </div>
                    <a href="upload_qb.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 170px;">UPLOAD QUESTION BANK</button></a>
                    <a href="admin_view_qp_deptwise.jsp"><button class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 210px;">VIEW QUESTION PAPERS</button></a>
                    <a href="index.jsp"><button type="button" class="btn btn-group-lg btn-primary" style="width: 250px;position: absolute;top: 250px;">LOGOUT</button></a>
                </div>
                <div class="col-lg-10 table-responsive" style="position: absolute;left: 281px;height: 582px;background-image: url(p1.jpeg);top: 150px;object-fit: cover;text-transform: uppercase;overflow: auto" >
                    <%
                        try {
                            
                            con = DB.getconnection();
                            String sql = "select * from questionbank";
                            ps=con.prepareStatement(sql);
                            rs = ps.executeQuery();
                            ResultSetMetaData rsmd = rs.getMetaData();
                    %>
                    <table class="table table-striped table-hover" width="100%" cellspacing="1" style="background-color: #f2f2f2;color: #0000cc;overflow: scroll;">
                        <thead class="thead-dark">
                            <tr>
                                <th><%=rsmd.getColumnName(1)%></th>
                                <th><%=rsmd.getColumnName(2)%></th>
                                <th><%=rsmd.getColumnName(3)%></th>
                                <th><%=rsmd.getColumnName(4)%></th>
                                <th><%=rsmd.getColumnName(5)%></th>
                                <th><%=rsmd.getColumnName(6)%></th>
                                <th>DOWNLOAD</th>
                            </tr>
                        <tbody>
                            <% while (rs.next()) {%>
                            <tr>
                                <td><%=rs.getInt(1)%></td>
                                <td><%=rs.getString(2)%></td>
                                <td><%=rs.getString(3)%></td>
                                <td><%=rs.getString(4)%></td>
                                <td><%=rs.getString(5)%></td>
                                <td><%=rs.getTimestamp(6)%></td>
                                <td><a href="db_download_qb?filename=<%=rs.getString(4)%>" ><button type="button" class="btn btn-success btn-group-lg">DOWNLOAD</button></a></td>
                            </tr>
                            <%   }
                                } catch (Exception e) {
                                    System.out.println(e);
                                }

                            %>  
                        </tbody>
                    </table>
                        <div>
                            <a href="getStuDet.jsp"><button class="btn btn-group-lg btn-warning">BACK</button></a>
                        </div>

                </div>
            </div>
        </div>
        <footer >
            &copy COPYRIGHTS RESERVED
        </footer>

    </body>
</html>

