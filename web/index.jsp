<%-- 
    Document   : index
    Created on : 12 Aug, 2019, 12:44:06 PM
    Author     : Lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Skcet Management</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link rel="stylesheet" type="text/css"  href="newcss.css">-->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="Jquery/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css"  href="styles.css">
    </head>
    <body>
        <header>
            <div>
                <img id="myImg" src="logo1.png" alt="mickey">
            </div>

            <div class="dropdown">
                <button class="dropbtn">LOGIN</button>
                <div class="dropdown-content">
                    <a href="login.jsp">ADMIN</a>
                    <a href="student_login.jsp">STUDENT</a>
                    <a href="staff_login.jsp">STAFF</a>
                </div>
            </div>
            <div>
                <a href="#"><button id="hit" type="submit" name="FAQ" >FAQ</button></a>
            </div>
        </header>
        <section>
            <article id="art">
                <div class="container">
                    <h2><b>our college</b></h2><br>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:50%;">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousal" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousal" data-slide-to="1" ></li>
                            <li data-target="#myCarousal" data-slide-to="2" ></li>
                        </ol>

                        <div class="carousel-inner">

                            <div class="item active"  >
                                <img src="images/b.png" alt="myPic" style="height: 380px;width: 100%;">
                            </div>

                            <div class="item" >
                                <img src="images/i1.png" alt="myPic" style="height: 380px;width: 100%;">
                            </div>

                            <div class="item">
                                <img src="images/i2.png" alt="myPic" style="height: 380px;width: 100%">
                            </div>

                        </div>

                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">previous</span>
                        </a>

                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">next</span>
                        </a>

                    </div>


                </div>
            </article>
            <article id="con">
                <div >
                    <h2><b>ABOUT :</b></h2>
                    <p style="color: black"> 
                        Sri Krishna College of Engineering and Technology is one of the premier institutions of Sri Krishna Trust,
                        established in the year 1998 with a mission of imparting high quality technical education to the aspiring students.
                        This self finance, autonomous institution accredited by NAAC with 'A' Grade (with 3.42 CGPA out of 4) and National Board of Accreditation is located
                        at a prominent place in Coimbatore city which can be easily accessible on the Palghat high way of NH 47. SKCET is one of the leading institutions in 
                        Tamil Nadu State that offers Under Graduate, Post Graduate and Research Programs in Engineering and Technology and in addition, the college has gained 
                        recognition for its uniquely tailored management education program.<br><br>
                        The Colleges offers 15 programs that include 7 Under Graduate programs and 8 Post Graduate programs and 4 of
                        the 9 academic departments were recognized as research centers with more than 5000 students and 250 faculties.
                        Over 1200 students are awarded with their degrees every year. The College attracts students from various parts 
                        of the state and from neighboring states and has emerged as trusted destination for quality technical education in this part of the state.<br><br>
                        Sri Krishna College of Engineering and Technology enjoys the patronage of top recruiters of national
                        and international reputation and the students are offered with multiple employment offers. 
                        The placement record of SKCET is on par with institutions that have national reputation. On the sides of bondage with alumni, 
                        the SKCET's Alumni Association has established a strong alumni network.<br><br>
                        SKCET has also signed various MOU's with many of the world's best Educational and Research giants,
                        and has also built it's partnership with many corporate bodies and industries. The college has carved a niche for research and development
                        by regularly publishing research articles in high impact journals, registering patents, involving in various research projects, organizing workshops,
                        conferences, seminars and symposiums.<br><br>
                        SKCET is a lush green campus, aesthetically built in a world class manner with well designed construction of buildings with 
                        state - of - art library, convention centre, auditorium, seminar halls, cafeteria, sports and games fields with fitness centers, etc,
                        A high tech infrastructure has been created with high configured computers, internet facility, smart board enabled class rooms, Google virtual class
                        rooms and Wi-Fi facility across the campus. There are separate hostels for boys and girls that can accommodate about 2500 students with a very comfortable
                        stay.<br><br>
                        SKCET makes the students life in the campus very interesting and purposeful.
                        The students are provided with opportunities to take part in sports and games, cultural programs, dramas, various club activities, 
                        National Service Scheme and Youth Red Cross. In addition, ample of opportunities are provided in participating and conducting seminars, 
                        workshops, symposiums etc. The living area of the campus provides banking and ATM facilities, book shops, gift shops, general stores, hotels,
                        cafeterias, health care centre etc., The College is committed in bringing out real life global engineers nurtured with best practices and ethical values 
                        and not mere engineering graduates. The College fulfills the commitment by following its Motto 'Perfection through Performance' tirelessly.
                    </p>
                </div>
            </article>

        </section>
        <footer>

            &copy copyrights reserved 

        </footer>

    </body>
</html>
