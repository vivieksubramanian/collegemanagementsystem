/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileUploadByStaff;


import com.servlet.db.DB;
import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;


import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
@WebServlet(name = "db_qb_uploadSf", urlPatterns = {"/db_qb_uploadSf"})
@MultipartConfig(fileSizeThreshold= 1024*1024*10 , maxFileSize=1024*1024*1000 , maxRequestSize=1024*1024*1000)
public class db_qb_uploadSf extends HttpServlet {
    
    PrintWriter out=null;
    Connection con=null;
    PreparedStatement ps=null;
    HttpSession session=null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try
        {
            out=response.getWriter();
            session = request.getSession(false);
            String folderName="qb";
            String uploadPath = request.getServletContext().getRealPath("")+File.separator+folderName;
            File dir=new File(uploadPath);
            if(!dir.exists())
            {
                dir.mkdirs();
            }
            Part filePart = request.getPart("file");
            String department = request.getParameter("department");
            String subject = request.getParameter("subject");
            String filename=filePart.getSubmittedFileName();
            String path=folderName+File.separator+filename;
            Timestamp added_date = new Timestamp(System.currentTimeMillis());
            System.out.println("fileName: " + filename);
            System.out.println("Path: "+ uploadPath);
            System.out.println("departmentName: "+ department);
            System.out.println("subject: "+ subject);
            InputStream is=filePart.getInputStream();
            Files.copy(is,Paths.get(uploadPath+File.separator+filename), StandardCopyOption.REPLACE_EXISTING);
            try
            {
                
                con=DB.getconnection();
                System.out.println("connection done");
                String sql="insert into questionbank(department,subject,filename,path,added_date) values(?,?,?,?,?)";
                ps = con.prepareStatement(sql);
                ps.setString(1,department);
                ps.setString(2,subject);
                ps.setString(3,filename);
                ps.setString(4,path);
                ps.setTimestamp(5, added_date);
                int status = ps.executeUpdate();
                if(status>0)
                {
                    session.setAttribute("filename", filename);
                    String msg=""+filename+" File uploaded successfully.....!!!";
                    request.setAttribute("msg",msg);
                    RequestDispatcher rd=request.getRequestDispatcher("/staff_upload_qb_success.jsp");
                    rd.forward(request, response);
                    System.out.println("File Uploaded successfully....");
                    System.out.println("uploaded path: "+uploadPath);
                }
                
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            } 
            finally
            {
                try
                {
                    if(ps!=null)
                            ps.close();
                    if(con!=null)
                        con.close();
                }
                catch(SQLException e)
                {
                    out.println(e);
                }
                
            }
            
        }
        catch(IOException | ServletException e)
        {
            out.println("Exception: "+e);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
